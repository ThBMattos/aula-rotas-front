import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    ContainerHome: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#6ea4fa'
    },
    ContainerProfile: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#916efa'
    },
    ContainerCart: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fa836e'
    },
    Button: {
      paddingTop: 10,
      paddingBottom: 10,
      paddingLeft: 20,
      paddingRight: 20,
      marginTop: 10,
      borderRadius: 5,
      backgroundColor: '#FFFFFF',
    },
    BackButton: {
      padding: 20,
      marginTop: 10,
      borderRadius: 10,
      backgroundColor: '#fc3d3d'
    },
});