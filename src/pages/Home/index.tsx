import React from  'react';
import { View, Text } from 'react-native';

import { styles } from '../../globalStyles';

const Home = () => {
    return(
        <View style={styles.ContainerHome}>
            <Text>Home</Text>
        </View>
    )
}

export default Home;